# CatFacts

This application was created as recruitment task for Miquido.

## Technologies used
- Kotlin
- Koin `DI Framework`
- Retrofit `HTTP client`
- Coroutines 
- Mockito
- JUnit